<?php
if ( function_exists('register_sidebar') ){
    register_sidebar(array(
    	'id' => 'sidebar',
    	'name' => 'Sidebar',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>'
    ));
}

function register_my_menu() {
register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

?>
