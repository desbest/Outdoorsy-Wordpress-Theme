<?php get_header(); ?>
					<div id="leftcol">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<h2><a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<?php edit_post_link('Edit this entry.', '<span class="post-meta">', '</span><div class="divider"></div>'); ?>
							<?php the_content('Read the rest of this Page'); ?>
						<?php endwhile; else: ?>
							<h2>Not Found</h2>
							<p class="center">Sorry, but you are looking for something that isn't here.</p><br />
							<?php include (TEMPLATEPATH . "/searchform.php"); ?>
						<?php endif; ?>
					</div>
					<div id="rightcol">
						<?php get_sidebar(); ?>
					</div>
<?php get_footer(); ?>
