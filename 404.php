<?php get_header(); ?>
					<div id="leftcol">
						<h2>404 - Page Not Found</h2>
						<p>Sorry, this page can not be located</p>
						<?php include(TEMPLATEPATH.'/searchform.php'); ?>
					</div>
					<div id="rightcol">
						<?php get_sidebar(); ?>
					</div>
<?php get_footer(); ?>
