<?php get_header(); ?>
					<div id="leftcol">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="post">
							<h2><a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<span class="post-meta"><?php the_time('F j, Y'); ?> - Posted by <?php the_author_link(); ?></span>
								<div class="divider"></div>
							<p class=�attachment�><?php echo wp_get_attachment_image( $post->ID, �large� ); ?>
<p><?php if ( !empty($post->post_content) ) ?> <div class="divider"></div><?php the_content(); ?><div class="divider"></div></p>
							<?php previous_image_link() ?>
							<?php next_image_link() ?>
							<div class="divider"></div>
								<div class="about-post">
									This entry was posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> and is filed under <?php the_category(', ') ?>. You can follow any responses to this entry through the <?php post_comments_feed_link('RSS 2.0'); ?> feed.			
									<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
										// Both Comments and Pings are open ?>
										You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(); ?>" rel="trackback">trackback</a> from your own site.
			
									<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
										// Only Pings are Open ?>
										Responses are currently closed, but you can <a href="<?php trackback_url(); ?> " rel="trackback">trackback</a> from your own site.
			
									<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
										// Comments are open, Pings are not ?>
										You can skip to the end and leave a response. Pinging is currently not allowed.
			
									<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
										// Neither Comments, nor Pings are open ?>
										Both comments and pings are currently closed.
			
									<?php } edit_post_link('Edit this entry.','',''); ?>
								</div>
							<div class="divider"></div>
						</div><?php comments_template(); ?>
						<?php endwhile; else: ?>
							<h2>Not Found</h2>
							<p class="center">Sorry, but you are looking for something that isn't here.</p><br />
							<?php include (TEMPLATEPATH . "/searchform.php"); ?>
						<?php endif; ?>
					</div>
					<div id="rightcol">
						<?php get_sidebar(); ?>
					</div>
<?php get_footer(); ?>

