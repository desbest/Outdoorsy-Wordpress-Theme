<?php get_header(); ?>
					<div id="leftcol">
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
						<div class="post">
							<h2><a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<span class="post-meta"><?php the_time('F j, Y'); ?> - Posted by <?php the_author_link(); ?> - <?php comments_popup_link('0 Comments', '1 Comment', '% Comments'); ?></span>
								<div class="divider"></div>
							<?php the_content(' '); ?>
							<span class="post-footer"><a href="<?php the_permalink(); ?>" title="Continue Reading <?php the_title(); ?>">Continue Reading...</a></span>
						</div><br /><br />
							<?php endwhile; ?>
								<div class="navigation">
									<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
									<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
								</div>
							<?php else : ?>
								<h2>Not Found</h2>
								<p class="center">Sorry, but you are looking for something that isn't here.</p>
								<?php include (TEMPLATEPATH . "/searchform.php"); ?>
							<?php endif; ?>
					</div>
					<div id="rightcol">
						<?php get_sidebar(); ?>
					</div>
<?php get_footer(); ?>
