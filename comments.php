<?php // Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
			?>

			<p class="nocomments">This post is password protected. Enter the password to view comments.</p>

			<?php
			return;
		}
	}
?>

<!-- You can start editing here. -->

<?php if ($comments) : ?>
	<h3><?php comments_number('0 Comments', '1 Comment', '% Comments' );?> </h3><a name="comments"></a>

	<ol id="commentlist">
	<?php foreach ($comments as $comment) : ?>
		<li id="comment-<?php comment_ID() ?>">
			<h5><?php comment_author_link() ?> says:</h5>
				<?php if ($comment->comment_approved == '0') : ?>
					<blockquote><em>Your comment is awaiting moderation.</em></blockquote><br />
				<?php endif; ?>
				
				<?php comment_text() ?>

				<div class="comment-meta">... on July <?php comment_date('F jS, Y') ?></div>
		</li>
	<?php endforeach; ?>
	</ol>
 <?php else : // this is displayed if there are no comments so far ?>
	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->
	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<h3>Comments are closed.</h3>
	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>
	<h3>Post a Comment</h3><a name="respond"></a>

	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
		<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
	<?php else : ?>

	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( $user_ID ) : ?>
			<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Log out &raquo;</a></p>
		<?php else : ?>
			<div class="comment-info">
				<input type="text" name="author" id="author" value="<?php if($comment_author == "") echo 'Name'; else echo $comment_author; ?>" onblur="if (this.value == '') {this.value = 'Name';}"  onfocus="if (this.value == 'Name') {this.value = '';}" tabindex="1" class="input" />
				<input type="text" name="email" id="email" value="<?php if($comment_author_email == "") echo 'Email'; else echo $comment_author_email; ?>" onblur="if (this.value == '') {this.value = 'Email';}"  onfocus="if (this.value == 'Email') {this.value = '';}" tabindex="2" class="input" />
				<input type="text" name="url" id="url" value="<?php if($comment_author_url == "") echo 'http://'; else echo $comment_author_url; ?>" onblur="if (this.value == '') {this.value = 'http://';}"  onfocus="if (this.value == 'http://') {this.value = '';}" tabindex="3" class="input" />
			</div>
		<?php endif; ?>
			<div class="comment-comment">
				<textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4" class="input" onblur="if (this.value == '') {this.value = 'Your Comment...';}"  onfocus="if (this.value == 'Your Comment...') {this.value = '';}">Your Comment...</textarea></p>
				<!--<p><small><strong>XHTML:</strong> You can use these tags:<code><?php echo allowed_tags(); ?></code></small></p>-->
					<br class="clear" /><br />
				<input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" class="submit right" />
				<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
			</div>
		<?php do_action('comment_form', $post->ID); ?>
	</form>

<?php endif; // If registration required and not logged in ?>

<?php endif; // if you delete this the sky will fall on your head ?>
