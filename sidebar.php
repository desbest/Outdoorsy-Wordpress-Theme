<ul id="sidebar">
	<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar') ) : ?>
		<li><h4>Search</h4>
			<ul>
				<li><?php include (TEMPLATEPATH . '/searchform.php'); ?></li>
			</ul>
		</li>
		<?php if (file_exists(TEMPLATEPATH.'/about.php')) : ?>
			<li><h4>About Us</h4>
				<ul>
					<li><?php include(TEMPLATEPATH.'/about.php'); ?></li>
				</ul>
			</li>
		<?php endif; ?>
		<li><h4>Categories</h4>
			<ul>
				<?php wp_list_categories('title_li='); ?>			
			</ul>
		</li>
		<li><h4>Archives</h4>
			<ul>
				<?php wp_get_archives('type=monthly'); ?>
			</ul>
		</li>
		<?php wp_list_bookmarks('title_li=Blogroll&title_before=<h4>&title_after=</h4>&categorized='); ?>
		<?php if ( is_home() || is_page() ) { ?>
			<li><h4>Meta</42>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
					<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
					<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
					<?php wp_meta(); ?>
				</ul>
			</li>
		<?php } ?>
	<?php endif; ?>
</ul>