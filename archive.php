<?php get_header(); ?>
					<div id="leftcol">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="post">
							<h2><a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<span class="post-meta"><?php the_time('F j, Y'); ?> - Posted by <?php the_author_link(); ?>- <?php comments_popup_link('0 Comments', '1 Comment', '% Comments'); ?></span>
								<div class="divider"></div>
							<?php the_excerpt('Continue Reading'); ?><br /><br />
						</div><?php comments_template(); ?>
						<?php endwhile; else: ?>
							<h2>Not Found</h2>
							<p class="center">Sorry, but you are looking for something that isn't here.</p><br />
							<?php include (TEMPLATEPATH . "/searchform.php"); ?>
						<?php endif; ?>
					</div>
					<div id="rightcol">
						<?php get_sidebar(); ?>
					</div>
<?php get_footer(); ?>

